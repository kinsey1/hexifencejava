
public class MoveSelector {
	
	Board initState;
	
	public MoveSelector(Board initState){
		this.initState = initState;  
	}
	
	
	
	/**
	 * @param board Input state
	 * @return Returns the x,y coordinates of a move which wont
	 * cause any next turn captures. Used for early game moves.
	 * Returns null if no move is found that wont cause captures.
	 */
	public int[] select_move(Board board){
		int xy[] = new int[2];	
		xy = selectEarlyMove(board);
		if(xy!=null)
			return xy;
		
		//TREE SEARCH HERE FOR BOARDS
		
		return xy;
	}
	
	
	public int[] selectEarlyMove(Board board){
		int xy[] = new int[2];
		
		for (int i = 0; i < board.positions.length;i++){
			if(board.checkMove(board.positions[i].x, board.positions[i].y) == 1){
				xy[0] = 0;
				xy[1] = 1;
				return xy;
			}
		}
		return null;
		
	}
	
	private int countOpenMoves(Board board){
		int count = 0;
		for (int i = 0; i < board.positions.length;i++){
			if(board.checkMove(board.positions[i].x, board.positions[i].y) == 1){
				count++;
			}
		}
		return count;
	}
	

}
