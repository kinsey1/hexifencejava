import java.io.IOException;

/**
 * @author Kinsey Reeves - kreeves
 * 
 * Briana Achtman - bachtman
 */

public class Main {
	public static void main(String args[]) throws InterruptedException {
		Input input = new Input();
		
		BoardGenerator boardgen = new BoardGenerator();
		/*
		try {
			for(int i = 0;i <100; i++){
				boardgen.writeFile(2,1);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int numBoard = 0;
		StringBuilder sb;
		
		for (int i = 0;i<100;i++){
			
			System.out.println("board : " + numBoard);
			sb = new StringBuilder();
			sb.append("boards/board_gen_");
			sb.append(numBoard);
			sb.append(".txt");
			
			String path = sb.toString();
			
			System.out.println(path);
			Board board = new Board(input.readFile(path), 4 * input.boardSize - 1);
			
			//board.printBoard();
			
			System.out.println(board.countFreeEdges());
			System.out.println(board.oneMoveCapture());
			System.out.println(board.oneTurnCapture());
			
			System.out.println("\n\n");
			numBoard++;
		}
		*/
	
		
		
		Board board = new Board(input.readFile("boards/board_gen_1.txt"), 4 * input.boardSize - 1);

		System.out.println(board.countFreeEdges());
		System.out.println(board.oneMoveCapture());
		System.out.println(board.oneTurnCapture());
		
		board.printBoard();
		for (int i = 0; i < board.positions.length;i++){
			
			System.out.println(board.checkMove(board.positions[i].x, board.positions[i].y));
		}
		
		
		
		
		
			
		
		
		
		
		//System.out.println(board.getNextHex(3,3,4,3)[0]);
		//System.out.println(board.getNextHex(3,3,4,3)[1]);



	}
}

