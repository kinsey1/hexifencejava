import java.io.BufferedWriter;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Random;

/**
 * @author Kinsey Reeves - kreeves
 * 
 * Briana Achtman - bachtman
 */

public class BoardGenerator {
	
	public static int numBoard = 0;
	
	
	
	/**
	 * @param size the N size of the board
	 * @param path Path to write the file (should be saved locally)
	 * @param completeness likelyhood of an edge being free, int between 1 and 10.
	 * @throws IOException 
	 */
	
	//this is unneeded, it exists for making eclipse testing more simple
	public void writeFile(int size, int completeness) throws IOException{
		System.out.println(numBoard);
		StringBuilder sb = new StringBuilder();
		sb.append("boards/board_gen_");
		sb.append(numBoard);
		sb.append(".txt");
		String path = sb.toString();
		numBoard++;
		
		size = 4*size-1;
		Random rand = new Random();
		int turn = 0;
		Writer writer = null;
		
		Input input = new Input();
		Board board = new Board(input.readFile("boards/2example.txt"), 4 * input.boardSize - 1);
		
		

		try {
		    writer = new BufferedWriter(new OutputStreamWriter(
		          new FileOutputStream(path), "utf-8"));
		    writer.write(2 + "\n");
		    for (int y =0; y<size;y++){
				for (int x=0;x<size;x++){
					//System.out.println("x: " + x + "y: " + y);
					//System.out.println(board.positions[y*size+x].type);
					int randy = rand.nextInt(10);
					
					if (board.positions[y*size+x].type==-1|| board.positions[y*size+x].type==1){
						writer.write("-");
					}else{
						if (randy>completeness){
							if((turn&1)==1)writer.write("R");
							else writer.write("B");
						}
						else{
							writer.write("+");
						}
					}
					if(x!=size-1)writer.write(" ");
					turn++;
				}
				writer.write("\n");
			}
		    
		   
		    
		} catch (IOException ex) {
		  // report
		} finally {
		   try {writer.close();} catch (Exception ex) {/*ignore*/}
		}
		
		
		
		
	}

}
