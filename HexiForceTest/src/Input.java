import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * @author Kinsey Reeves - kreeves
 * 
 * Briana Achtman - bachtman
 */
/**
 * Input class
 *
 */
public class Input {

	public int boardSize; // size of the board
	public Position[] positions;

	/**
	 * @param path
	 *            A string of the path of the input file
	 * @return an array of positions of type Position
	 */
	// this is unneeded, it exists for making eclipse testing more simple
	public Position[] readFile(String path) {
		// Reads the file and returns the size of the board. Saving
		Scanner in = null;
		Scanner lines = null;
		int posIndex = 0;
		int numLines = 0;
		// File opening using scanner--change to System.in for release
		try {
			in = new Scanner(new File(path));
			lines = new Scanner(new File(path));
		} catch (FileNotFoundException e) {
			System.out.println("invalid input");
			e.printStackTrace();
		}

		while (lines.hasNextLine()) {
			numLines++;
			lines.nextLine();
		}

		if (in != null) {
			int y = 0;

			boardSize = Integer.parseInt(in.nextLine());
			positions = new Position[(boardSize * 4 - 1) * (boardSize * 4 - 1)]; // the
																					// size
																					// of
																					// the
																					// board

			if ((boardSize * 4 - 1) != (numLines - 1)) {
				System.out.println("invalid input");
				System.exit(1);
			}

			while (in.hasNextLine()) {

				String line = in.nextLine();
				line = line.replace(" ", "");
				if (line.length() > (boardSize * 4 - 1)) {
					System.out.println("invalid input");
					System.exit(1);
					break;
				}

				for (int x = 0; x < line.length(); x++) {
					char pos = line.charAt(x);

					if (pos != '+' && pos != 'R' && pos != 'B' && pos != '-') {
						System.out.println("invalid input");
						System.exit(1);
					}
					positions[posIndex++] = new Position(x, y, pos);
				}

				y++;

			}
			if (in.hasNextLine()) {
				System.out.println("invalid input");
				System.exit(1);
			}

		}
		return positions;
	}

	public Position[] parseBoard() {
		// Reads the file and returns the size of the board. Saving
		Scanner in = null;

		int posIndex = 0;

		try {
			in = new Scanner(System.in);

		} catch (Exception e) {
			System.out.println("invalid input");
			e.printStackTrace();
		}

		if (in != null) {
			int y = 0;

			boardSize = Integer.parseInt(in.nextLine());
			positions = new Position[(boardSize * 4 - 1) * (boardSize * 4 - 1)]; // the
																					// size
																					// of
																					// the
																					// board

			while (posIndex < (boardSize * 4 - 1) * (boardSize * 4 - 1)) {
				String line = in.nextLine();
				line = line.replace(" ", "");
				if (line.length() > (boardSize * 4 - 1)) {
					System.out.println("invalid input"); // any line is too big
					System.exit(1);
					break;
				}

				for (int x = 0; x < line.length(); x++) {
					char pos = line.charAt(x);
					if (pos != '+' && pos != 'R' && pos != 'B' && pos != '-') {
						System.out.println("invalid input");
						System.exit(1);
					}

					positions[posIndex++] = new Position(x, y, pos);
				}
				y++;

			}

			if (in.hasNextLine() && !(in.nextLine().isEmpty())) {
				System.out.println("invalid input");
				System.exit(1);
			}

		}
		return positions;
	}

}
