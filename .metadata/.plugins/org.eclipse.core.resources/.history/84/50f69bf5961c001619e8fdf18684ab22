import java.io.PrintStream;
import java.util.List;

public class KRPlayer implements Player, Piece{

	
	private int boardSize;
	private int piece;
	private int opponent;
	private int oppCaptured;
	private int playerCaptured;
	private int totalHexagons;
	private boolean lateGame;
	
	MoveSelector moveSelect;
	
	PrintStream output;
	
	public Board currBoard;
	public Input input = new Input();
	
	
	@Override
	public int init(int n, int p) {
		
		
		this.boardSize = 4*n-1;
		this.piece = p;
		this.lateGame = false;

		if(p==1){
			opponent = 2;
		}else if(p==2){
			opponent = 1;
		}else{
			return -1;
		}
		if(n==2){
			currBoard = new Board(input.readFile("../Boards/2example.txt"), n);
		}else if(n==3){
			currBoard = new Board(input.readFile("../Boards/3example.txt"), n);
		}else{
			return -1;
		}
		
		currBoard.getWinner();
		printBoard(output);
		totalHexagons = countHexagons();
		
		
		return 0;
	}

	@Override
	public Move makeMove() {
		
		
		Move move = null;
		
		/*This is done so that if moveselector has a 'next best move' 
		 * created using oneTurnCapture then it will get that without 
		 * searching. Otherwise it will generate a new moveSelector 
		 * for that board
		 * 
		 */
		if (moveSelect==null || lateGame == false){
			moveSelect = new MoveSelector(currBoard, piece);
			move = moveSelect.selectEarlyMove();
		}

		if(move == null){
			lateGame = true;
			move = moveSelect.selectLateMove();
		}
		
		applyMove(move);
		return move;
	}

	/**
	 * @param move
	 * applys a given move to the board and checks 
	 * if its caused a capture
	 */
	private void applyMove(Move move) {
		if(move.P == BLUE){
			currBoard.positions[move.Row*boardSize+move.Col].owner = Board.B_EDGE;
			updateCapturedHex(move.Col, move.Row, Board.B_HEX);
		}

		if(move.P==RED){
			currBoard.positions[move.Row*boardSize+move.Col].owner = Board.R_EDGE;
			updateCapturedHex(move.Col, move.Row, Board.R_HEX);
		}
		
	}

	@Override
	public int opponentMove(Move m) {
		
		//System.out.println("updating opponent move with " + m.Col + m.Row);
		int captured = 0;
		if(!isLegalMove(m.Col, m.Row))return -1;
		
		
		if (m.P == BLUE){
			currBoard.positions[m.Row*boardSize+m.Col].owner = 'B';
			captured = updateCapturedHex(m.Col, m.Row, 'b');
			oppCaptured+=captured;
			if(captured >=1)return 1;
			
		}else if(m.P==RED){
			currBoard.positions[m.Row*boardSize+m.Col].owner = 'R';
			captured = updateCapturedHex(m.Col, m.Row, 'r');
			oppCaptured+=captured;
			
			if(captured >=1)return 1;

		}
		return 0;
	}

	@Override
	public int getWinner() {
		int redCaptured = 0;
		int blueCaptured = 0;
		int free = 0;
		for(int i=0;i<currBoard.positions.length;i++){
			if(currBoard.positions[i].owner=='F'){
				return 0;
			}
			if(currBoard.positions[i].type == 1){
				if(currBoard.positions[i].owner == 'n')
					free++;
				if(currBoard.positions[i].owner == Board.R_HEX)
					redCaptured++;
				if(currBoard.positions[i].owner == Board.B_HEX)
					blueCaptured++;
				
			}
			
			
		}
		if(redCaptured+blueCaptured == totalHexagons){
			if(redCaptured > blueCaptured)
				return 2;
			else
				return 1;
		}
		return 0;
	}

	@Override
	public void printBoard(PrintStream output) {
		System.out.println("");
		output.append("\n");
		if(piece == 1){
			output.append("KR PLAYER = BLUE - PLAYER " + piece + "\n");
			System.out.println("KR PLAYER = BLUE - PLAYER " + piece);
		}else{
			output.append("KR PLAYER = RED - PLAYER " + piece +"\n");
			System.out.println("KR PLAYER = RED - PLAYER " + piece);
		}
		

		System.out.print("  ");
		output.append("  ");
		for(int a = 0; a <(boardSize); a++){
			output.append(a + " ");
			System.out.print(a + " ");
		}
		System.out.println();
		output.append("\n");
		for(int y = 0; y<(boardSize);y++){
			System.out.print(y + " ");
			output.append(y + " ");
			for(int x=0; x<(boardSize);x++){
				
				System.out.print(currBoard.positions[y*boardSize+x].owner + " ");
				output.append(currBoard.positions[y*boardSize+x].owner + " ");
			}
			System.out.println();
			output.append("\n");
		}
		output.flush();
	}
	
	
	private boolean isLegalMove(int x, int y){
		if(currBoard.positions[y*boardSize+x].type!=0)return false;
		if(currBoard.positions[y*boardSize+x].owner!=Board.FREE)return false;
		
		return true;
		
	}
	
	private int countHexagons(){
		int count = 0;
		for (int i=0;i<currBoard.positions.length;i++){
			if(currBoard.positions[i].type==1)count++;
		}
		return count;
	}
	
	/**
	 * Updates the board with hexagons owners.
	 * @param x -coord of taken edge cell
	 * @param y -coord of taken edge 
	 * @param player The players character. e.g. 'B'
	 * @return
	 * 
	 * Returns 0 if no hexagon is captured, 1 if one is, 2 if 2 are.
	 */
	private int updateCapturedHex(int x, int y, char player){

		int captured = 0;
		if (Board.isOdd(y)) {
			
			if(currBoard.countHexEdges(x+1, y)==0){
				
				currBoard.positions[y*boardSize+(x+1)].owner = player;
				captured++;
			}

			if(currBoard.countHexEdges(x-1, y)==0){
				currBoard.positions[y*boardSize+(x-1)].owner = player;
				captured++;
			}
			
			
		} else if (Board.isEven(y) && Board.isOdd(x)) {
			if(currBoard.countHexEdges(x, y+1)==0){
				currBoard.positions[(y+1)*boardSize+(x)].owner = player;
				captured++;
			}
				
			if(currBoard.countHexEdges(x, y-1)==0){
				currBoard.positions[(y-1)*boardSize+(x)].owner = player;
				captured++;
			}
			
		} else if (Board.isEven(y) && Board.isEven(x)) {
			
			if(currBoard.countHexEdges(x+1, y+1)==0){
				currBoard.positions[(y+1)*boardSize+(x+1)].owner = player;
				captured++;
			}
			
			if(currBoard.countHexEdges(x-1, y-1)==0){
				
				currBoard.positions[(y-1)*boardSize+(x-1)].owner = player;
				captured++;
			}

		}
		return captured;
	}

}
