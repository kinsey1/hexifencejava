import java.util.LinkedList;

/**
 * @author Kinsey Reeves - kreeves Briana Achtman - achtmanb
 * 
 *         Class containing board manipulation functions. The Positions Array
 *         contains all details of the board and all functions in this class can
 *         be used to gather relevant data of the board.
 *
 */

public class Board {

	private static final int SIDES = 6;
	
	public final static char FREE = 'F';
	public final static char B_EDGE = 'B';
	public final static char R_EDGE = 'R';
	public final static char B_HEX = 'b';
	public final static char R_HEX = 'r';
	public final static char NUL_SPACE = 'n';
	
	//public boolean isCritical;
	
	public Position[] positions;
	public int size;
	public boolean full;

	public Board(Position[] positions, int size) {

		this.positions = positions;
		this.size = 4*size-1;
		
	}

	/**
	 * @return Returns the number of free edges in the board. This is also the
	 *         number of moves possible.
	 * 
	 */

	public int countFreeEdges() {
		int freeEdges = 0;
		for (int i = 0; i < positions.length; i++) {
			if (positions[i].type == 0 && positions[i].owner == FREE) {

				freeEdges++;
			}
		}
		return freeEdges;
	}

	/**
	 * @return returns the number of edges which can be captured in one move NOT
	 *         one turn. (placement of one edge) It does this by: if a hexagon
	 *         has one free edge; it gets the free edges position and then
	 *         checks the adjoining hexagon (which shares this free edge). If
	 *         the adjoinig hexagon's free edge count is 1, this means they
	 *         share an empty edge. Meaning two cells can be captured in one
	 *         move NOTE : i made sure the edges were the same but this is not
	 *         necessary
	 */
	public int oneMoveCapture() {

		int captures = 0;

		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++) {

				if (isHex(x, y) && countHexEdges(x, y) == 1) {

					int edgeX = findFreeEdge(x, y)[0];
					int edgeY = findFreeEdge(x, y)[1];
					// gets the free edge

					if (isOutsideEdge(edgeX, edgeY)) {
						if (captures == 0)
							captures = 1;
						continue;
					}
					if (isOdd(edgeY)) {
						if ((countHexEdges(edgeX + 1, edgeY) == 1 && countHexEdges(edgeX - 1, edgeY) == 1)) {
							return 2;
						}
					} else if (isEven(edgeY) && isOdd(edgeX)) {
						if ((countHexEdges(edgeX, edgeY + 1) == 1 && countHexEdges(edgeX, edgeY - 1) == 1))
							return 2;
					} else if (isEven(edgeY) && isEven(edgeX)) {
						if ((countHexEdges(edgeX + 1, edgeY + 1) == 1 && countHexEdges(edgeX - 1, edgeY - 1) == 1))
							return 2;
					}
					if (captures == 0)
						captures = 1;
				}
			}
		}
		return captures;
	}

	/**
	 * @return Returns the amount of hexagons that can be captured in one TURN.
	 *         It checks for hexagons with one edge free, or ones that could be
	 *         captured in multiple moves (along a stream of hexagons and adds
	 *         them to a linked list. Otherwise, it could be a stream of
	 *         hexagons. It goes along hexagones with less than 2 free
	 *         edges(which originated from a 1 free edge hexagon) and adds these
	 *         positions to a linked list. The list is checked for duplicates
	 *         and any are removed. (there will always be a duplicate as any
	 *         stream forwards will be found in reverse). This should be done
	 *         more efficiently
	 */
	public LinkedList<Position> oneTurnCapture() {

		LinkedList<Position> capturables = new LinkedList<Position>();

		int nextHexX;
		int nextHexY;
		int edgeX;
		int edgeY;
		int nextEdgeX;
		int nextEdgeY;

		int nextHex[];
		int nextEdge[];


		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++) {

				if (!isHex(x, y))
					continue;
				if (countHexEdges(x, y) == 1) {
					edgeX = findFreeEdge(x, y)[0];
					edgeY = findFreeEdge(x, y)[1];

					// System.out.println("\n\nx " + x + "y " + y);

					if (isOutsideEdge(edgeX, edgeY)) {
						capturables.add(positions[y * size + x]);

						continue;
					} else {

						capturables.add(positions[y * size + x]);

						nextHexX = getNextHex(x, y, -1, -1)[0];
						nextHexY = getNextHex(x, y, -1, -1)[1];

						while (countHexEdges(nextHexX, nextHexY) <= 2) {

							// System.out.println("while");

							capturables.add(positions[nextHexY * size + nextHexX]);

							if (countHexEdges(nextHexX, nextHexY) == 1) {
								break;
							}

							if (isOutsideEdge(edgeX, edgeY))
								break;

							nextEdge = findFreeDoubleEdge(nextHexX, nextHexY, edgeX, edgeY);
							nextEdgeX = nextEdge[0];
							nextEdgeY = nextEdge[1];

							if (isOutsideEdge(nextEdgeX, nextEdgeY)) {
								break;
							}

							// System.out.println(getNextHex(1,1,1,2)[0]);
							// System.out.println(getNextHex(1,1,1,2)[1]);

							// System.out.println("hX " + nextHexX + " hY " +
							// nextHexY + " eX " + edgeX + " eY" + edgeY);

							nextHex = getNextHex(nextHexX, nextHexY, edgeX, edgeY);

							nextHexX = nextHex[0];
							nextHexY = nextHex[1];

							// System.out.println("nextHexX " + nextHexX + "
							// nextHexY " + nextHexY);

							// if(!isHex(nextHexX, nextHexY))break;

							// System.out.println("here2");

							if (nextHexX == -1 || nextHexY == -1)
								break;

							// System.out.println("here2");

							// System.out.println("nextHexX " + nextHexX + "
							// nextHexY " + nextHexY);

							edgeX = nextEdgeX;
							edgeY = nextEdgeY;

							// System.out.println("here3");

							// if the next hexagon in the stream has only one
							// free edge,
							// then break
							if (countHexEdges(nextHexX, nextHexY) == 1) {
								capturables.add(positions[nextHexY * size + nextHexX]);
								break;
							} else if (countHexEdges(nextHexX, nextHexY) > 2) {
								break;
							}
							// System.out.println("here4");

						}
					}
				}
			}
		}

		for (int i = 0; i < capturables.size(); i++) {
			// System.out.println(capturables.get(i).x);
			// System.out.println(capturables.get(i).y);

			for (int j = i + 1; j < capturables.size(); j++) {
				if (capturables.get(i).equals(capturables.get(j))) {
					capturables.remove(j);
				}
			}
		}

		return capturables;

	}

	/**
	 * @param x
	 * @param y
	 * @return Checks if the hexagon has fully taken edges. Returns the amount
	 *         of edges that a hexagon has free
	 *         If the hex is out of bounds it returns -1
	 * 
	 */

	public int countHexEdges(int x, int y) {
		int edges = 0;

		if (!isHex(x, y) || outOfBounds(x, y)) {
			edges = -1;
			return edges;// not a hexagon or out of bounds
		}
		// checking all 6 surrounding edges
		
		if (positions[y * size + (x + 1)].owner == FREE)
			edges++;
		if (positions[(y) * size + (x - 1)].owner == FREE)
			edges++;
		if (positions[(y + 1) * size + (x)].owner == FREE)
			edges++;
		if (positions[(y - 1) * size + (x)].owner == FREE)
			edges++;
		if (positions[(y + 1) * size + (x + 1)].owner == FREE)
			edges++;
		if (positions[(y - 1) * size + (x - 1)].owner == FREE)
			edges++;
		return edges;
	}

	/**
	 * @param x
	 * @param y
	 * @return Returns the x and y coordinate of an edge given the x and y
	 *         coordinate of a hexagon with one free edge
	 */
	private int[] findFreeEdge(int x, int y) {
		int[] xy = new int[2];

		if (positions[y * size + (x + 1)].owner == FREE) {
			xy[0] = x + 1;
			xy[1] = y;
		}

		else if (positions[(y) * size + (x - 1)].owner == FREE) {
			xy[0] = x - 1;
			xy[1] = y;
		}

		else if (positions[(y + 1) * size + (x)].owner == FREE) {
			xy[0] = x;
			xy[1] = y + 1;
		}

		else if (positions[(y - 1) * size + (x)].owner == FREE) {
			xy[0] = x;
			xy[1] = y - 1;
		}

		else if (positions[(y + 1) * size + (x + 1)].owner == FREE) {
			xy[0] = x + 1;
			xy[1] = y + 1;
		}

		else if (positions[(y - 1) * size + (x - 1)].owner == FREE) {
			xy[0] = x - 1;
			xy[1] = y - 1;
		}

		return xy;
	}



	/*
	 * 
	 */
	/**
	 * Checks if a move makes positions capturable used for taking moves in the
	 * early stages of the game. 
	 * @param x
	 * @param y
	 * @return
	 * Returns 1 if its a viable move - no possible captures by opponent.
	 * 
	 * Returns 2 if two adjacent cells can be captured(same as one move capture).
	 * 
	 * Returns -1 if its not a possible move (taken)
	 * 
	 * Returns 3 if a move is now captured or already captured
	 */
	public int checkMove(int x, int y) {
		int num = 0;
		
		int adj1=-1;
		int adj2=-1;

		
		if (positions[y*size+x].type!=0 | 
				positions[y*size+x].owner!=FREE)
			return -1;		
		
		if (isOdd(y)) {
			
			if(isHex(x+1,y)) 
				adj1=countHexEdges(x + 1, y);
			if(isHex(x-1,y)) 
				adj2=countHexEdges(x - 1, y);
			
		} else if (isEven(y) && isOdd(x)) {

			if(isHex(x,y-1))
				adj1=countHexEdges(x, y-1);
			if(isHex(x,y+1))
				adj2=countHexEdges(x, y+1);
			
		} else if (isEven(y) && isEven(x)) {
			
			
			
			if(isHex(x+1,y+1))
				adj1=countHexEdges(x + 1, y+1);
				
			if(isHex(x-1,y-1))
				adj2=countHexEdges(x - 1, y-1);
	
		}
		//System.out.println("-------------------");

		
		
		if(adj1==1 | adj2==1)return 2; //possible capture here
		else if(adj1==2 | adj2 ==2)return 0; //not a good move
		else if(adj1>2 && adj2>2 || (adj1 > 2 && adj2 == -1) || (adj2 > 2 && adj1 == -1))return 1; //a good move
		//System.out.println("here");

		return 0;
	}
	
	

	/**
	 * @param x
	 *            - x position of hexagon
	 * @param y
	 *            - y position of hexagon
	 * @param prevEdgeX
	 *            - previous edge x (input -1 for hexagon with one free edge)
	 * @param prevEdgeY
	 *            - previous edge y (input -1 for hexagon with one free edge)
	 * @return Returns the adjacent hexagon which shares an edge with a
	 *         capturable hexagon. It does so by checking whether the free edge
	 *         is odd/even x and y and can therefore calculate its adjacent
	 *         hexagons. Returning the hexagon that is not the hexagon passed in
	 *         with x,y. T
	 */

	public int[] getNextHex(int x, int y, int prevEdgeX, int prevEdgeY) {

		// System.out.println("x " + x + " y " + y + " prev x " + prevEdgeX + "
		// prev y " + prevEdgeY);

		int xy[] = new int[2];

		if (!isHex(x, y)) {
			xy[0] = -1;
			xy[1] = -1;
			return xy;
		}
		

		int freeEdgeX;
		int freeEdgeY;

		int nextHexX = -1;
		int nextHexY = -1;
		
		if (prevEdgeX > 0 && prevEdgeY > 0 && countHexEdges(x, y) > 1) {
			freeEdgeX = findFreeDoubleEdge(x, y, prevEdgeX, prevEdgeY)[0];
			freeEdgeY = findFreeDoubleEdge(x, y, prevEdgeX, prevEdgeY)[1];
		} else {
			freeEdgeX = findFreeEdge(x, y)[0];
			freeEdgeY = findFreeEdge(x, y)[1];
		}



		if (isOdd(freeEdgeY)) { // If y is odd, the adjacent cells are either
								// left or right.
			if (freeEdgeX + 1 == x && freeEdgeY == y) {
				nextHexX = freeEdgeX - 1;
				nextHexY = freeEdgeY;
			} else {
				nextHexX = freeEdgeX + 1;
				nextHexY = freeEdgeY;
			}
		} else if (isEven(freeEdgeY) && isOdd(freeEdgeX)) {
			if (freeEdgeX == x && freeEdgeY + 1 == y) {
				nextHexX = freeEdgeX;
				nextHexY = freeEdgeY - 1;
			} else {
				nextHexX = freeEdgeX;
				nextHexY = freeEdgeY + 1;
			}

		} else if (isEven(freeEdgeY) && isEven(freeEdgeX)) {
			if (freeEdgeX + 1 == x && freeEdgeY + 1 == y) {
				nextHexX = freeEdgeX - 1;
				nextHexY = freeEdgeY - 1;
			} else {
				nextHexX = freeEdgeX + 1;
				nextHexY = freeEdgeY + 1;
			}
		}
		xy[0] = nextHexX;
		xy[1] = nextHexY;



		if (!isHex(xy[0], xy[1])) {
			xy[0] = -1;
			xy[1] = -1;
		}

		return xy;
	}

	/* HELPER FUNCTIONS */

	/**
	 * @param x
	 *            - x coord of hexagon
	 * @param y
	 *            - y coord of hexagon
	 * @param prevEdgeX
	 *            - the previous edge from the last free hexagon
	 * @param prevEdgeY
	 *            - the previous edge from the last free hexagon
	 * @return Returns the edge of a hexagon of 2 free edges. Moving through
	 *         capturable hexagones It takes the previous edge, being the edge
	 *         we have just "captured" so it can find the next single edge to
	 *         take. Returns -1 for both if hex has one free edge
	 */

	private int[] findFreeDoubleEdge(int x, int y, int prevEdgeX, int prevEdgeY) {

		int[] xy = new int[2];
		xy[0] = -1;
		xy[1] = -1;
		Position prevEdge = positions[prevEdgeY * size + prevEdgeX];

		if (!prevEdge.equals(positions[y * size + (x + 1)]) && (positions[y * size + (x + 1)].owner == FREE)) {
			xy[0] = x + 1;
			xy[1] = y;
		}

		else if (!prevEdge.equals(positions[y * size + (x - 1)]) && positions[(y) * size + (x - 1)].owner == FREE) {
			xy[0] = x - 1;
			xy[1] = y;
		}

		else if (!prevEdge.equals(positions[(y + 1) * size + (x)]) && positions[(y + 1) * size + (x)].owner == FREE) {
			xy[0] = x;
			xy[1] = y + 1;
		}

		else if (!prevEdge.equals(positions[(y - 1) * size + (x)]) && positions[(y - 1) * size + (x)].owner == FREE) {
			xy[0] = x;
			xy[1] = y - 1;
		}

		else if (!prevEdge.equals(positions[(y + 1) * size + (x + 1)])
				&& positions[(y + 1) * size + (x + 1)].owner == FREE) {
			xy[0] = x + 1;
			xy[1] = y + 1;
		}

		else if (!prevEdge.equals(positions[(y - 1) * size + (x - 1)])
				&& positions[(y - 1) * size + (x - 1)].owner == FREE) {
			xy[0] = x - 1;
			xy[1] = y - 1;
		}

		return xy;
	}

	boolean isHex(int x, int y) {
		
		if (x > size-1 || y > size-1 || x < 0 || y < 0){

			return false;
		}

		if (positions[y * size + x].type != 1) {
			//System.out.println(x + " " + y);
			//System.out.println("here");
			return false;
		}
		return true;
	}

	private boolean outOfBounds(int x, int y) {
		if (x >= size-1 || x < 0)
			return true;
		if (y >= size-1 || y < 0)
			return true;
		return false;
	}

	static boolean isOdd(int a) {
		if ((a & 1) == 1)
			return true;
		return false;
	}

	static boolean isEven(int a) {
		if (a % 2 == 0)
			return true;
		return false;
	}

	private boolean isPos(int a) {
		if (a >= 0)
			return true;
		else
			return false;
	}

	private boolean isOutsideEdge(int x, int y) {
		if (x == -1 || y == -1)
			return true;

		if (x == 0 || y == 0 || y == size - 1 || x == size - 1)
			return true;
		if (positions[y * size + (x + 1)].type == -1)
			return true;
		if (positions[y * size + (x - 1)].type == -1)
			return true;
		return false;

	}
	
	/**
	 * @return returns 1 if red has won, 2 if blue has won,
	 * and 0 if the game is not over and -1 for error
	 */
	public int getWinner(){
		int red = 0;
		int blue = 0;
		int free = 0;
		
		for(int y = 1; y < size; y+=2){
			for(int x = 1; x < size; x+=2){
				if(positions[y*size+x].type == 1){
					if(positions[y*size+x].owner == B_EDGE)blue++;
					else if (positions[y*size+x].owner == R_EDGE)red++;
					else if (positions[y*size+x].owner == FREE)free++;

				}
			}
		}
		
		
		if (free!=0)return 0;
		if(blue>red)return 2;
		if(red>blue)return 1;
		return -1;
		
	}

	public void printBoard() {
		for (int i = 0; i < positions.length; i++) {
			System.out.println("x " + positions[i].x + " y " + positions[i].y + " type  " + positions[i].type + " owner " + positions[i].owner);
		}
	}

}
