/**
 * @author Kinsey Reeves - kreeves
 * Briana Achtman - bachtman
 */
public class Position {

	public int type; // 0 for an edge, 1 for hex, -1 for void
	public char owner; // f, free, r, red, b, blue
	public int x;
	public int y;

	/**
	 * @param x
	 * @param y
	 * @param spot
	 */
	public Position(int x, int y, char spot) {
		// checks type of the position
		this.x = x;
		this.y = y;

		if (spot == '-') { // hexagon or void
			this.type = -1;
			this.owner = 'n';
		} else if (spot == '+') { // free edge
			this.owner = 'F';
			this.type = 0;
		} else if (spot == 'B') {// blue edge
			this.owner = 'B';
			this.type = 0;
		} else if (spot == 'R') {// red edge
			this.owner = 'R';
			this.type = 0;
		}

	}

	public boolean equals(Position pos) {
		if (pos.x == this.x && pos.y == this.y) {
			return true;
		}
		return false;
	}

}
